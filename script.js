// console.log("Hello World! This is from External JS");

/*Writing a comment*/
	// single line 	ctrl + /
	// multi-line comment 	ctrl + shift + /


/*
	Syntax and Statement

Syntax 
	- set of rules of how codes should be written correctly

Statement
	- set of instructions, ends with semicolon

*/

//alert("Good afternoon");


/*
	Variables
		- a container that holds value
		- using let keyword
*/

	let myName = "Joy";
	// console.log(myName);

/* Anatomy 
	
	**Declaration:
		let <variable name>
			- declaration
			- declaring a variable
			- cannot re-declare same variable name under same scope

	Example:
			let car;
			console.log(car);

			let car = "mercedez";
			console.log(car);

	**Initialization

		- initializing a variable with a value
Example:
	let phone = "iPhone";
	console.log(phone);



	**Re-assignment
		- assigning new value to a variable using equal operator.

Example:		
	myName = "Gab";
	console.log(myName);
*/

// Why do you think variable is important?
	// reusable
	
// let brand = "Asus";
// brand = "Mac";

// console.log(brand);
// console.log(brand);
// console.log(brand);
// console.log(brand);
// console.log(brand);
// console.log(brand);
// console.log(brand);
// console.log(brand);
// console.log(brand);
// console.log(brand);
	

//console.log(lastName);
	// without let keyword, return will be "NOT DEFINED"


// let firstName;
// console.log(firstName);
	// returns "UNDEFINED"


/*
	Naming Convention

	1. Case sensitive
		let color = "pink";
		let Color = "blue";

		console.log(color);
		console.log(Color);

	2. Varibale name must be relevant to its actual value

		let value = "Robin";

		let goofThief = "Robin";
		let bird = "Robin";

	3. Camel Case Notation

	let capitalcityofthephilippines = "Manila City";
	
	//this is readable
	let capitalCityOfThePhilippines = "Manila City";

*/
	
	let year3 = 3;
	//console.log(year3);

	//not accepted by JS:
		// let 3year = 3;
		// console.log(3year);

		// let @year = 3;
		// console.log(@year);

	let _year = 3;
	//console.log(_year);

	let $year = 3;
	//console.log($year);


	//single & double quotes are accepted
	let flower = "rose";
	// console.log(flower);

	let flower2 = 'sunflower';
	// console.log(flower2);

//example:

	//console.log("She said, "Hello there!". ");
	//console.log("She said, 'Hello there!'. ");
	//console.log('She said, "Hello there!". ');

	//console.log('Hello, I\'m Joy!');

	//ES6 updates
		//template literals - using backticks ``

	//console.log(`She said, "Hello there!".`);


/*
	Constant
		- a type of variable that holds value but it cannot be changed nor re-assigned
*/

	const PI = 3.14;
	//console.log(PI);

	/*
		PI = 14;
		console.log(PI);
		- reassignment is not allowed
	*/
	/*
		const boilingPoint;
		console.log(boilingPoint);
		- declaration must have value else it will be error
	*/

/*
	MINI ACTIVITY

		Declare variables called
			country
			continent
			population

		Assign their values according to your own country (population as of 2020)

		Log their values to the console
*/


/*
	Data Types
	
	
	1. String
		- sequence of characters
		- they are always wrapped with quotes or backticks
		- if no quotes or backticks, JS will think of it as another variable
	let food = `sinigang`;
	let Name = `Joy Pague`;


	2. Number
		- whole number(integer) & decimals(float)


	let x = 4;
	let y = 8;

	let result = x + y;
	console.log(result);

	let a = `2`;
	let b = `8`;
	let c = `3`;

	let newResult = a + b + c;	//concatenation
	console.log(newResult);

	let secondResult = b - a - c;	//subtraction
	console.log(secondResult);


	3. Boolean
		- values TRUE or FALSE
	let isEarly = true;
	console.log(isEarly);

	let areLate = false;
	console.log(areLate)


	4. Undefined
		- variable has been declared however no value yet
		- empty value
	let job;
	console.log(job);


	5. Null
		- empty value
		- it is used to represent an empty value


	6. BigInt()
		- large integers more than the data type can hold


	7. Object
		- one variable can handle/contain several different types of data
			- it is wrapped with curly braces
			- has property & value

	let user = {
		firstName: "Dave",
		lastName: "Supan",
		email: [
			"ds@mail.com", 
			"dave@gmail.com", 
			"dsupan@yahoo.com"
		],
		age: 16,
		isStudent: true,
		spouse: null
	}


	//**special type of object
		//Array
			// collection of related data
			// enclosed with square brackets
	let fruits = ["apple", "banana", "strawberry"];
	let grades = [89, 90, 92, 97, 95];

*/
/*
	typeof Operator
		- evaluates what type of data and returns a string
*/	
	let animal = "dog";
	let age = 16;
	let isHappy = true;
	let him;
	let spouse = null;
	let admin = {
		name: "Admin",
		isAdmin: true
	}
	let ave = [83.5, 89.6, 94.2];


	// console.log(typeof animal);
	// console.log(typeof age);
	// console.log(typeof isHappy);
	// console.log(typeof him);
	// console.log(typeof spouse);
	// console.log(typeof admin);
	// console.log(typeof ave);



/*
	Functions
		- reusable
		- convenience bec it helps us save time than repeating same task over and over again
*/

	function sayHello(){
		console.log("Hello World");
		console.log("Hello World");
		console.log("Hello World");
		console.log("Hello World");
		console.log("Hello World");
		console.log("Hello World");
		console.log("Hello World");
		console.log("Hello World");
		console.log("Hello World");
		console.log("Hello World");
	}

	// sayHello();
	// console.log("_____________")
	// sayHello();
	// console.log("_____________")
	// sayHello();

/*
	Syntax of a function:

		function <fName>(){
		
			//statement / code block
		}



	Anatomy of a function:

		1. Declaration
			- function keyword
			- function name + parenthesis
				- parameters inside the parenthesis
			- curly braces
				- determines its codeblock
				- statements are written inside the codeblock

		2. Invocation
			- invokes / calls the function
			- by invoking the function, it executes the codeblock

			- function name + parenthesis
				- arguments inside the parenthesis

*/
	//Example:
				//parameter
	function greeting(name){

		console.log(`Hi ${name}!`);
	}

			//argument
	// greeting("Angelito");
	// greeting("Mauro");



	//Example:

	function getProduct(x, y){

		console.log(`The product of ${x} and ${y} is ${x * y}`)
	}

	getProduct(5, 7);
	getProduct(8, 3);


/*
	Mini Activity

	Create a function to get the total sum of three numbers

	Display the output in the console

	Screenshot both code and output and send it to group chat
*/

	function totalSum(num1, num2, num3){
		console.log(num1 + num2 + num3);

	}

	// totalSum(2, 4, 6);


	/* function with return keyword */

	function sayName(fName, lname){

		return `Hi, my name is ${fName} ${lname}`
	}

	//first method
	console.log(sayName("Roxanne", "Talampas"))

	//second method
	let result = sayName("Amiel", "Canta")
	console.log(result)

/*
	Mini Activity

	Create a function that will accept name and age.
	Display the return of the function in the console.

	It should look like this.

	Hi, I'm <your name>
	My age <age> + 10 is <your age>

*/

	function showInfo(name, age){
		console.log(`Hi, I'm ${name}. My age ${age} + 10 is ${age + 10}`)
		
		return (`Hi, I'm ${name}. My age ${age} + 10 is ${age + 10}`)
	}

	showInfo("Joy Pague", 28);
